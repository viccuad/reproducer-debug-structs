#![allow(unused_imports)]
#![allow(dead_code)]
use anyhow::{anyhow, Result};
use policy_fetcher::verify::config::read_verification_file;
use std::{
    env, path::Path,
};

fn main() -> Result<()> {
    // let args: Vec<String> = env::args().collect();
    // let verification_filepath = Path::new(&args[1]);

    let verification_filepath = Path::new("./verification-config.yml");

    match read_verification_file(verification_filepath) {
        Err(e) => Err(anyhow!(
            "error while loading verification info from {:?}: {}",
            verification_filepath,
            e
        )),
        Ok(vs) => {
            println!("{:#?}", vs);
            Ok(())
        }
    }
}
